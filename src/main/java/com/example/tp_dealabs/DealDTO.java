package com.example.tp_dealabs;

import java.time.LocalDateTime;
import java.util.Date;

public class DealDTO {

	public long id;
	public Integer nbDegres;
	public String titre;
	public String user;
	public String imgUrl;
	public LocalDateTime date;
	public String shopLink;
	public String compagny;
	public double priceOld;
	public double priceNew;
	public String promoCode;
	public String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getNbDegres() {
		return nbDegres;
	}

	public void setNbDegres(Integer nbDegres) {
		this.nbDegres = nbDegres;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getShopLink() {
		return shopLink;
	}

	public void setShopLink(String shopLink) {
		this.shopLink = shopLink;
	}

	public String getCompagny() {
		return compagny;
	}

	public void setCompagny(String compagny) {
		this.compagny = compagny;
	}

	public double getPriceOld() {
		return priceOld;
	}

	public void setPriceOld(double priceOld) {
		this.priceOld = priceOld;
	}

	public double getPriceNew() {
		return priceNew;
	}

	public void setPriceNew(double priceNew) {
		this.priceNew = priceNew;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
