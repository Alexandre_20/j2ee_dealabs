package com.example.tp_dealabs;

import com.example.tp_dealabs.service.DealBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/deal")
public class DealController {

	@Autowired
	private DealBO dealBO;

	@CrossOrigin(origins = "*")
	@RequestMapping(method = RequestMethod.GET)
	public List<DealDTO> getAll(){

		return dealBO.getDeals();
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public DealDTO getSingleDeal(@PathVariable Integer id){

		return dealBO.findSingleDeal(id);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(method = RequestMethod.POST)
	public DealDTO createSingleDeal(@RequestBody DealDTO dealDTO) {

		return dealBO.createSingleDeal(dealDTO);
	}

}