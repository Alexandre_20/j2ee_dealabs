package com.example.tp_dealabs.DAO;

import javax.persistence.*;
import javax.transaction.Transactional;

import com.example.tp_dealabs.DO.TemperatureDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
@Transactional
public class TemperatureDAO {

	@Autowired
	private EntityManager entityManager;

	public ArrayList<TemperatureDO> selectAll(){
		Query q = entityManager.createQuery("from TemperatureDO ");

		return (ArrayList<TemperatureDO>) q.getResultList();
	}

	public TemperatureDO selectOne(long id){
		Query q = entityManager.createQuery("from TemperatureDO where id = :deal_id");

		q.setParameter("temperature_id", id);

		return (TemperatureDO) q.getSingleResult();
	}
}