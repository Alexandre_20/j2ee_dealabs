package com.example.tp_dealabs.DAO;

import javax.persistence.*;
import javax.transaction.Transactional;

import com.example.tp_dealabs.DO.DealDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
@Transactional
public class DealDAO {

	@Autowired
	private EntityManager entityManager;

	public ArrayList<DealDO> selectAll(){
		Query q = entityManager.createQuery("from DealDO ");

		return (ArrayList<DealDO>) q.getResultList();
	}

	public DealDO selectOne(long id){
		Query q = entityManager.createQuery("from DealDO where id = :id");

		q.setParameter("id", id);

		return (DealDO) q.getSingleResult();
	}

	public DealDO create(DealDO dealDO) {
		entityManager.persist(dealDO);
		return dealDO;
	}
}