package com.example.tp_dealabs.DO;

import javax.persistence.*;

@Entity
@Table(name="tbl_temperature")
public class TemperatureDO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public long id;

	@Column(name = "value")
	public Integer value;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "fk_user")
	public UserDO user;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "fk_deal")
	public DealDO deal;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public UserDO getFK_User() {
		return user;
	}

	public void setFK_User(UserDO FK_User) {
		this.user = user;
	}

	public DealDO getFK_Deal() {
		return deal;
	}

	public void setFK_Deal(DealDO FK_Deal) {
		this.deal = deal;
	}
}
