package com.example.tp_dealabs.DO;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="tbl_user")
public class UserDO{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public long id;

	@Column(name = "pseudo")
	public String pseudo;

	@Column(name = "first_name")
	public String firstName;

	@Column(name = "last_name")
	public String lastName;

	@Column(name = "password")
	public String password;

	@OneToMany(cascade = {CascadeType.ALL})//mappedBy = "user")
	private List<TemperatureDO> temperaturesDO;

	@OneToMany(cascade = {CascadeType.ALL})//, mappedBy = "user")
	private List<DealDO> dealsDO;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<TemperatureDO> getTemperatureDOList() {
		return temperaturesDO;
	}

	public void setTemperatureDOList(List<TemperatureDO> temperatureDOList) {
		this.temperaturesDO = temperatureDOList;
	}

	public List<DealDO> getNewDealDOList() {
		return dealsDO;
	}

	public void setNewDealDOList(List<DealDO> dealDOList) {
		this.dealsDO = dealDOList;
	}
}