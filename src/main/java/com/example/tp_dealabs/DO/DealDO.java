package com.example.tp_dealabs.DO;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="tbl_deal")
public class DealDO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public long id;

	@Column(name = "date")
	public LocalDateTime date;

	@Column(name = "description")
	public String description;

	@Column(name = "img_url")
	public String imgUrl;

	@Column(name = "price_new")
	public double priceNew;

	@Column(name = "price_old")
	public double priceOld;

	@Column(name = "promo_code")
	public String promoCode;

	@Column(name = "shop_link")
	public String shopLink;

	@Column(name = "shop_name")
	public String shopName;

	@Column(name = "title")
	public String title;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "fk_creator")
	public UserDO user;

	@OneToMany(cascade = {CascadeType.ALL})//, mappedBy = "deal")
	private List<TemperatureDO> temperatures;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopLink() {
		return shopLink;
	}

	public void setShopLink(String shopLink) {
		this.shopLink = shopLink;
	}

	public double getPriceOld() {
		return priceOld;
	}

	public void setPriceOld(double priceOld) {
		this.priceOld = priceOld;
	}

	public double getPriceNew() {
		return priceNew;
	}

	public void setPriceNew(double priceNew) {
		this.priceNew = priceNew;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UserDO getUser() {
		return user;
	}

	public void setUser(UserDO user) {
		this.user = user;
	}

	public List<TemperatureDO> getTemperatures() {
		return temperatures;
	}

	public void setTemperatures(List<TemperatureDO> temperatures) {
		this.temperatures = temperatures;
	}

}