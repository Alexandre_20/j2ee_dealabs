package com.example.tp_dealabs.service;

import com.example.tp_dealabs.DAO.DealDAO;
import com.example.tp_dealabs.DO.DealDO;
import com.example.tp_dealabs.DealDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DealBO {

	@Autowired
	private DealDAO dealDAO;

	@Autowired
	private TemperatureBO temperatureBO;

	public List<DealDTO> getDeals() {
		ArrayList<DealDTO> dealDTOArray = new ArrayList<DealDTO>();
		ArrayList<DealDO> dealDOArray = (ArrayList<DealDO>) dealDAO.selectAll();

		for (DealDO deal : dealDOArray) {
			DealDTO tempDTO = new DealDTO();
			tempDTO.setId(deal.getId());
			tempDTO.setTitre(deal.getTitle());
			tempDTO.setUser(deal.getUser().getPseudo());
			tempDTO.setNbDegres(temperatureBO.getTemperature(deal));
			tempDTO.setDate(deal.getDate());
			tempDTO.setImgUrl(deal.getImgUrl());
			dealDTOArray.add(tempDTO);
		}

		return dealDTOArray;
	}


	public DealDTO findSingleDeal(Integer id) {
		DealDO dealDO = dealDAO.selectOne(id);
		DealDTO dealDTO = new DealDTO();

		dealDTO.setId(dealDO.getId());
		dealDTO.setTitre(dealDO.getTitle());
		dealDTO.setCompagny(dealDO.getShopName());
		dealDTO.setShopLink(dealDO.getShopLink());
		dealDTO.setPriceOld(dealDO.getPriceOld());
		dealDTO.setPriceNew(dealDO.getPriceNew());
		dealDTO.setPromoCode(dealDO.getPromoCode());
		dealDTO.setNbDegres(temperatureBO.getTemperature(dealDO));
		dealDTO.setImgUrl(dealDO.getImgUrl());
		dealDTO.setDescription(dealDO.getDescription());
		//dealDTO.setUser(newDealDO.getUser());
		dealDTO.setDate(dealDO.getDate());

		return dealDTO;
	}

	public DealDTO createSingleDeal(DealDTO dealDTO) {
		DealDO dealDO = new DealDO();

		dealDO.setId(dealDTO.getId());//Useless
		dealDO.setTitle(dealDTO.getTitre());
		dealDO.setShopName(dealDTO.getCompagny());
		dealDO.setShopLink(dealDTO.getShopLink());
		dealDO.setPriceOld(dealDTO.getPriceOld());
		dealDO.setPriceNew(dealDTO.getPriceNew());
		dealDO.setPromoCode(dealDTO.getPromoCode());
		//newDealDO.setTemperature(dealDTO.getNbDegres());
		dealDO.setImgUrl(dealDTO.getImgUrl());
		dealDO.setDescription(dealDTO.getDescription());
		//newDealDO.setCreator(dealDTO.getUser());
		dealDO.setDate(dealDTO.getDate());

		DealDO dealDOCreate = dealDAO.create(dealDO);
		dealDTO.setId(dealDOCreate.getId());

		return dealDTO;
	}
}
