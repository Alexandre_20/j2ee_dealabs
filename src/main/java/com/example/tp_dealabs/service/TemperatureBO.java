package com.example.tp_dealabs.service;

import com.example.tp_dealabs.DO.DealDO;
import com.example.tp_dealabs.DO.TemperatureDO;
import org.springframework.stereotype.Service;

@Service
public class TemperatureBO {

	public Integer getTemperature(final DealDO newdealDO){
		Integer totalTemperature = 0;

		for (final TemperatureDO currentTemp :  newdealDO.getTemperatures()){
			totalTemperature = totalTemperature + currentTemp.getValue();
		}

		return totalTemperature;
	}
}
