var express = require('express');
const dealController = require("../controller/dealController");
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res, next) {
  res.render('index', { title: '' });
});

router.get('/Deal/:iddeal', dealController.deal_infos);

router.get('/Create', dealController.deal_create);
router.post('/Create', dealController.post_create);

module.exports = router;