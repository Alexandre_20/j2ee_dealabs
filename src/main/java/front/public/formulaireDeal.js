const queryBtn = document.getElementById("envoi");
const queryBtnAnnuler = document.getElementById("annuler");
let form = document.getElementById("createDeal")

queryBtn.addEventListener('click', function () {

    var title_form = form.elements['titre'].value;
    var description_form = form.elements['description'].value;
    var oldPrice_form = form.elements['priceOld'].value;
    var newPrice_form = form.elements['newPrice'].value;
    var compagny_form = form.elements['compagny'].value;
    var promoCode_form = form.elements['promoCode'].value;
    var imgUrl_form = form.elements['imgUrl'].value;
    var current_date = Date();

    var dataForm = [title_form, description_form, oldPrice_form, newPrice_form, compagny_form, promoCode_form, imgUrl_form];

    //TODO : Verifier si tous les champs ont été remplis
    if(verifyForm(dataForm)) {
        let data = {
            titre: title_form,
            compagny: compagny_form,
            shopLink: null,
            priceOld: oldPrice_form,
            priceNew: newPrice_form,
            promoCode: promoCode_form,
            nbDegres: 0,
            user: "Default",
            date: null,
            imgUrl: imgUrl_form,
            description: description_form
        };

        post(JSON.stringify(data));
    }
    else{
        alert('Veillez remplir tous les champs');
    }
})

queryBtnAnnuler.addEventListener('click', function (){

    document.getElementById("titre").value = "";
    document.getElementById("description").value = "";
    document.getElementById("priceOld").value = "";
    document.getElementById("newPrice").value = "";
    document.getElementById("compagny").value = "";
    document.getElementById("promoCode").value = "";
    document.getElementById("imgUrl").value = "";

})

function post(dataDeal){

    var headers = new Headers();
    headers.append("Content-Type", "application/json");

    var requestOptions = {
        method: 'POST',
        body: dataDeal,
        headers: headers,
        redirect: 'follow'
    };

    console.log(requestOptions)

    fetch("http://localhost:8080/deal", requestOptions)
}

function verifyForm(dataForm){
    var boolean = true;

    dataForm.forEach(element => {
        if(element == ""){
            boolean = false;
        }
    });

    return boolean;
}