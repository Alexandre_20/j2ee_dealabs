var requestOptions = {
    method: 'GET',
    redirect: 'follow'
};

fetch("http://localhost:8080/deal", requestOptions)
    .then(response => response.json())
    .then(response => {
        console.log("Generate");
        generate(response);
    })
    .catch(error => console.log('error', error));

function generate(dealJson){
    let table = document.getElementsByTagName("tbody")[0];

    tbody = "<tbody>";
    dealJson.forEach(element => {
        tbody += `<tr>
          <td> <img src="${element["imgUrl"]}" style="width:50px;height:60px;"> </td>
          <td>${element["id"]}</td>
          <td> <a href="deal/${element["id"]}">${element["titre"]}</a></td>
          <td>${element["nbDegres"]}°</td>
          <td>${element["user"]}</td>
          <td>${element["date"]}</td>
          <td> <button onclick= "window.location.href = '${element["shopLink"]}'"> Voir </button></td>
      </tr>`;
    });
    tbody += "</tbody>";
    table.innerHTML = tbody;
}


const queryBtn = document.querySelector('.btn');

queryBtn.addEventListener('click', function (){
})