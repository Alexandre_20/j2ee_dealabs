var requestOptions = {
    method: 'GET',
    redirect: 'follow'
};

var idDeal = document.getElementById("IdDeal").outerText;
console.log(idDeal.outerText);

fetch("http://localhost:8080/deal/" + idDeal, requestOptions)
    .then(response => response.json())
    .then(response => {
        console.log("GenerateSingleDeal");
        console.log(response);
        generateSingleDeal(response);
    })
    .catch(error => console.log('error', error));

function generateSingleDeal(dealJson){
    let table = document.getElementsByTagName("tbody")[0];

    var remiseprc = Math.round(((dealJson["priceOld"] - dealJson["priceNew"]) / dealJson["priceOld"] ) *100 );

    tbody = "<tbody>";
    tbody += `

        <div class="container">
            <div class="row">
                <div class="col">   
                    <img src="${dealJson["imgUrl"]}" style="width:50px;height:60px;> 
                </div>
                
                <div class="col">
                    ${dealJson["nbDegres"]}°
                    ${dealJson["titre"]}
                    ${dealJson["user"]}
                </div>
                
                <div class="col"> 
                    ${dealJson["date"]}
                    <button onclick= "window.location.href = '${dealJson["shopLink"]}'"> Voir </button>
                </div>
            </div>
            
            <div class="row">
                ${dealJson["promoCode"]}
            </div>
            
            <div class="row">
                ${dealJson["priceOld"]} --> ${dealJson["priceNew"]} (${remiseprc} % de remise)
            </div>
            
            <div class="row">
                ${dealJson["description"]}
            </div>

        </div>



      </tr>`;

    tbody += "</tbody>";
    table.innerHTML = tbody;
}